# CDL++ template

## Creating config.json
For the configuration step to work, you have to create a `config.json` first:

    {
        "token": "<bot token>"
    }

you can add your own settings and access them via the `env.settings` object

## Building

    mkdir build
    cd build
    cmake ..
    make -j$(nproc)


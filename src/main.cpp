#include <string>
#include "cdltypes.hpp"
using namespace CDL;


void echo(CMessage msg, CChannel channel, cmdargs& args) {
    channel->send("Echo from "+msg->author->username+": "+args[0]);
}

int main(int argc, char **argv) {
    register_command("echo", echo, 1);

    handlers::get_prefix = [] (CChannel, auto cb) {
        cb("c-");
    };

    using namespace intent_vals;
    CDL::main(argc, argv, GUILD_MESSAGES | DIRECT_MESSAGES);
}

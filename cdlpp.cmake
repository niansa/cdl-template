function(use_cdlpp target cdlpp_src_dir)
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(cdlpp REQUIRED IMPORTED_TARGET cdlpp)

    target_link_libraries(${PROJECT_NAME} PUBLIC
            PkgConfig::cdlpp)
endfunction()
